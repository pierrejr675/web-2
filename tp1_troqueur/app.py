from flask import Flask, render_template, request, redirect, url_for, session
import bd
import utils
import re
import os
from compte import bp_compte
from objet import bp_objet

app = Flask(__name__)
app.register_blueprint(bp_compte, url_prefix='/compte')
app.register_blueprint(bp_objet, url_prefix='/objet')
app.secret_key = "Cette chaîne servira pour l'encryption de la session. \
                  Elle doit être générée aléatoirement"

if __name__ == '__main__':
    app.run()


@app.route('/')
def index():
    """Accueil"""
    objets = utils.executeSqlQuery('SELECT * FROM objets ORDER BY id DESC LIMIT 5')
    return render_template('accueil.jinja', objets=objets, titre="Marché Au Puce")

@app.errorhandler(400)
def bad_request(e):
    return render_template('erreur/erreur_400.jinja'), 400

@app.errorhandler(401)
def unauthorized(e):
    return render_template('erreur/erreur_401.jinja'), 401

@app.errorhandler(403)
def unauthorized(e):
    return render_template('erreur/erreur_403.jinja'), 403

# Gestion 500
@app.errorhandler(500)
def internal_server_error(e):
    return render_template('erreur/erreur_500.jinja'), 500
