"""
Fonction Utiles
"""
import hashlib
import re
from flask import abort, session
import bd



def executeSqlQuery(query, params=None, fetchone=False):
    """Je exécute les requêtes sql ici (Plus simple)"""
    with bd.creer_connexion() as conn:
        with conn.get_curseur() as curseur:
            if params:
                curseur.execute(query, params)
            else:
                curseur.execute(query)

            if fetchone:
                return curseur.fetchone()
            else:
                return curseur.fetchall()

#validation du titre
def valider_titre(titre):
    if not titre:
        return "Le titre ne peut pas être vide."
    if not re.match("^[A-Za-z]+$", titre):
        return "Le titre doit contenir uniquement des lettres alphabétiques."
    if len(titre) < 1 or len(titre) > 50:
        return "Le titre doit avoir entre 1 et 50 caractère."
    return None

#validation de la description
def valider_description(description):
    if not description:
        return "La description ne peut pas être vide."
    if len(description) < 5 or len(description) > 2000:
        return "La description doit avoir entre 5 et 2000 caractère."
    return None

# validation du nom de fichier de la photo
def valider_nom_fichier_photo(nom_fichier):
    if not re.match("^[A-Za-z0-9-_.]{6,50}$", nom_fichier):
        return "Le nom de fichier de la photo doit avoir entre 6 et 50 caractères alphanumériques, incluant le tiret et le point."
    return None

# validation de la catégorie
def valider_categorie(categorie_id):
    executeSqlQuery('SELECT id FROM categories WHERE id = %s', (categorie_id,))
    return None


#hasher mdp

def hacher_mdp(mdp_en_clair):
    """Prend un mot de passe en clair et lui applique une fonction de hachage"""
    return hashlib.sha512(mdp_en_clair.encode('utf-8')).hexdigest()
#Validation mdp + email
def emailEstValide(email):
    pattern = r'^[\w\.-]+@[\w\.-]+\.\w+$'
    return re.match(pattern, email)
def passwordEstStrong(password):
    return len(password) >= 8 and any(c.isupper() for c in password) and any(c.islower() for c in password) and any(c.isdigit() for c in password)

