from flask import Flask, Blueprint, session, render_template, request, redirect, url_for, flash
import bd
import utils



bp_compte = Blueprint('compte', __name__)


@bp_compte.route('/authentifier', methods=['GET', 'POST'])
def authentifier():
    """Pour effectuer une authentification"""
    erreur = False
    courriel = request.form.get("courriel", default="")
    if (request.method == 'POST') :
        mdp = utils.hacher_mdp(request.form.get("mdp"))
        with bd.creer_connexion() as conn:
            utilisateur = bd.authentifier(
                conn,
                courriel,
                mdp
            )
            erreur = (not utilisateur)
            if not erreur:
                session["utilisateur"] = utilisateur
                return redirect('/', code=303)
    return render_template(
        'compte/authentifier.jinja',
        courriel=courriel,
        erreur=erreur
    )
    return render_template('compte/authentifier.jinja')


@bp_compte.route("/creer", methods=['GET', 'POST'])
def creer():
    champs = {
        "courriel" : {
            "valeur" : request.form.get("courriel", default=""),
            "en_erreur" : False
        },
        "nom" : {
            "valeur" : request.form.get("nom", default=""),
            "en_erreur" : False
        },
        "mdp" : {
            "en_erreur" : False
        },
    }
    if(request.method == 'POST') :
        mdp = request.form.get("mdp")
        if mdp != request.form.get("mdp2"):
            champs["mdp"]["en_erreur"] = True
        else :
            mdp = utils.hacher_mdp(mdp)
            with bd.creer_connexion() as conn:
                utilisateur = {
                    "courriel": champs["courriel"]["valeur"],
                    "nom": champs["nom"]["valeur"],
                    "mdp": mdp
                }
                utilisateur["id_utilisateur"] = bd.ajouter_utilisateur(conn, utilisateur)
                session["utilisateur"] = utilisateur
                return redirect('/', code=303)

    return render_template('compte/creer.jinja', champs=champs)

@bp_compte.route('/ajouter_utilisateur', methods=['POST', 'GET'])
def ajouter_utilisateur():
    if 'utilisateur' not in session or not session['utilisateur']['admin']:
        return render_template('erreur/erreur_403.jinja'), 403
    if request.method == 'POST':
        courriel = request.form.get("courriel")
        nom = request.form.get("nom")
        mdp = request.form.get("mdp")
        if not utils.emailEstValide(courriel):
            flash("Le courriel n'est pas valide.", "error")
        elif not utils.passwordEstStrong(mdp):
            flash("Le mot de passe doit contenir au moins 8 caractères, une lettre majuscule, une lettre minuscule et un chiffre.", "error")
        else:
            mdp_hache = utils.hacher_mdp(mdp)
            nouvel_utilisateur = {
                "courriel": courriel,
                "nom": nom,
                "mdp": mdp_hache
            }
            with bd.creer_connexion() as conn:
                nouvel_utilisateur["id_utilisateur"] = bd.ajouter_utilisateur(conn, nouvel_utilisateur)
            return redirect(url_for('compte.liste_utilisateurs'))
    return render_template("admin/ajoutUtilisateur.jinja")

@bp_compte.route('/liste_utilisateurs')
def liste_utilisateurs():
    if 'utilisateur' not in session or not session['utilisateur']['admin']:
        return render_template('erreur/erreur_403.jinja'), 403
    with bd.creer_connexion() as conn:
            utilisateurs = bd.liste_utilisateurs(conn)
    return render_template('admin/listeUtilisateur.jinja', utilisateurs=utilisateurs)

@bp_compte.route('/supprimer_utilisateur/<int:id>', methods=['GET', 'POST'])
def supprimer_utilisateur(id):
    if 'utilisateur' not in session or not session['utilisateur']['admin']:
        return render_template('erreur/erreur_403.jinja'), 403
    with bd.creer_connexion() as conn:
        utilisateur = bd.get_utilisateur_par_id(conn, id)
        if not utilisateur:
            return render_template('erreur_404.jinja'), 404
        if request.method == 'POST':
            bd.supprimer_utilisateur(conn, id)
            return redirect(url_for('compte.liste_utilisateurs'))
    return "Action non autorisée ou inconnue", 403

@bp_compte.route('/deconnecter', methods=['GET'])
def deconnecter():
    """Pour se déconnecter"""
    session.clear()
    return redirect("/")
