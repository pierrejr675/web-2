"""
Connexion à la BD
"""
import types
import contextlib
import mysql.connector
import utils


@contextlib.contextmanager
def creer_connexion():
    """Pour créer une connexion à la BD"""
    conn = mysql.connector.connect(
        user="garneau",
        password="qwerty123",
        host="127.0.0.1",
        database="tp1_troqueur",
        raise_on_warnings=True
    )

    # Pour ajouter la méthode getCurseur() à l'objet connexion
    conn.get_curseur = types.MethodType(get_curseur, conn)

    try:
        yield conn
    except Exception:
        conn.rollback()
        raise
    else:
        conn.commit()
    finally:
        conn.close()


@contextlib.contextmanager
def get_curseur(self):
    """Permet d'avoir les enregistrements sous forme de dictionnaires"""
    curseur = self.cursor(dictionary=True)
    try:
        yield curseur
    finally:
        curseur.close()


def ajouter_utilisateur(conn, utilisateur):
    "Ajoute un utilisateur dans la BD"
    with conn.get_curseur() as curseur:
        curseur.execute(
            "INSERT INTO utilisateur (courriel, nom, mdp) VALUES (%(courriel)s, %(nom)s, %(mdp)s)",
            utilisateur
        )
        return curseur.lastrowid

def authentifier(conn, courriel, mdp):
    """Retourne un utilisateur avec le courriel et le mdp"""
    with conn.get_curseur() as curseur:
        curseur.execute(
            "SELECT * FROM utilisateur WHERE courriel=%(courriel)s and mdp=%(mdp)s",
            {
                "courriel": courriel,
                "mdp": mdp
            }
        )
        return curseur.fetchone()


def est_admin(conn, utilisateur_id):
    """Vérifie si un utilisateur est un administrateur"""
    with conn.get_curseur() as curseur:
        curseur.execute(
            "SELECT admin FROM utilisateur WHERE id=%(utilisateur_id)s",
            {
                "utilisateur_id": utilisateur_id
            }
        )
        resultat = curseur.fetchone()
        if resultat is not None:
            return resultat['admin'] == 1
        return False


#admin only
def liste_utilisateurs(conn):
    return utils.executeSqlQuery("SELECT * FROM utilisateur")

def get_utilisateur_par_id(conn, utilisateur_id):
    return utils.executeSqlQuery("SELECT * FROM utilisateur WHERE id_utilisateur=%(utilisateur_id)s", {"utilisateur_id": utilisateur_id})

def supprimer_utilisateur(conn, utilisateur_id):
    return utils.executeSqlQuery("DELETE FROM utilisateur WHERE id_utilisateur=%(utilisateur_id)s", {"utilisateur_id": utilisateur_id})
