from flask import Blueprint, render_template, request, redirect, url_for, session, abort, flash
import bd
import utils
import os



bp_objet = Blueprint('objet', __name__)

@bp_objet.route('/listeObjet')
def objet():
    objets = utils.executeSqlQuery('SELECT * FROM objets')
    return render_template('listeObjet.jinja', objets=objets)


@bp_objet.route('/objet/<int:id>')
def detail_objet(id):
    query = '''SELECT o.*, c.description AS categorie_description FROM objets o JOIN categories c ON o.categorie = c.id WHERE o.id = %s'''
    objet = utils.executeSqlQuery(query, (id,), fetchone=True)
    if objet is None:
        return render_template('erreur_404.jinja'), 404
    return render_template('detail.jinja', objet=objet)


@bp_objet.route('/ajout', methods=['GET', 'POST'])
def ajouter_objet():
    if 'utilisateur' not in session:
        return abort(401)
    erreur = None
    if request.method == 'POST':
        titre = request.form['titre']
        description = request.form['description']
        photo = request.files['photo']
        categorie_id = request.form['categorie']
        user_id = session['utilisateur']['id_utilisateur']
        erreur_titre = utils.valider_titre(titre)
        erreur_description = utils.valider_description(description)
        erreur_categorie = utils.valider_categorie(categorie_id)
        if erreur_titre or erreur_description or erreur_categorie:
            erreur = {
                'titre': erreur_titre,
                'description': erreur_description,
                'categorie': erreur_categorie
            }
        else:
            if photo and photo.filename != '':
                nom_fichier = photo.filename
                chemin_fichier = os.path.join('static/images/objets', nom_fichier)
                photo.save(chemin_fichier)
            else:
                # image par default
                nom_fichier = 'C-21.jpg'
            utils.executeSqlQuery(
                'INSERT INTO objets (titre, description, photo, categorie, id_utilisateur) VALUES (%s, %s, %s, %s, %s)',
                (titre, description, nom_fichier, categorie_id, user_id))
            return redirect(url_for('index'))
    categorie = utils.executeSqlQuery('SELECT id, description FROM categories')
    return render_template('ajout.jinja', objet=None, categorie=categorie, erreur=erreur)

@bp_objet.route('/modifier_objet/<int:id>', methods=['GET', 'POST'])
def modifier_objet(id):
    objet = utils.executeSqlQuery('SELECT * FROM objets WHERE id = %s', (id,), fetchone=True)
    categories = utils.executeSqlQuery('SELECT id, description FROM categories')
    if objet is None:
        about(404)
    if 'utilisateur' not in session:
        abort(401)
    user_id = session['utilisateur']['id_utilisateur']
    if 'admin' in session and session['admin']:
        if request.method == 'POST':
            titre = request.form['titre']
            description = request.form['description']
            categorie_id = request.form['categorie']
            utils.executeSqlQuery('UPDATE objets SET titre=%s, description=%s, categorie=%s WHERE id=%s',
                                  (titre, description, categorie_id, id))
            return redirect(url_for('objet.detail_objet', id=id))
        return render_template('modifier_objet.jinja', objet=objet, categories=categories)
    if objet['id_utilisateur'] != user_id:
        abort(403)
    if request.method == 'POST':
        titre = request.form['titre']
        description = request.form['description']
        categorie_id = request.form['categorie']
        utils.executeSqlQuery('UPDATE objets SET titre=%s, description=%s, categorie=%s WHERE id=%s',
                              (titre, description, categorie_id, id))
        return redirect(url_for('objet.detail_objet', id=id))
    return render_template('modifier_objet.jinja', objet=objet, categories=categories)

@bp_objet.route('/supprimer_objet/<int:id>', methods=['POST'])
def supprimer_objet(id):
    objet = utils.executeSqlQuery('SELECT * FROM objets WHERE id = %s', (id,), fetchone=True)
    if objet is None:
        abort(404)
    if 'utilisateur' not in session:
        abort(401)
    user_id = session['utilisateur']['id_utilisateur']
    if 'admin' in session and session['admin']:
        utils.executeSqlQuery('DELETE FROM objets WHERE id = %s', (id,))
        flash("L'objet a été supprimé avec succès!", "success")
        return redirect(url_for('objet.liste_objet'))
    if objet['id_utilisateur'] != user_id:
        abort(403)
    utils.executeSqlQuery('DELETE FROM objets WHERE id = %s', (id,))
    flash("L'objet a été supprimé avec succès!", "success")
    return redirect(url_for('objet.liste_objet'))

@bp_objet.route('/troquer_objet/<int:id>', methods=['GET', 'POST'])
def troquer_objet(id):
    objet = utils.executeSqlQuery('SELECT * FROM objets WHERE id = %s', (id,), fetchone=True)
    if objet is None:
        abort(404)
    if 'utilisateur' not in session:
        abort(401)
    user_id = session['utilisateur']['id_utilisateur']
    if objet['id_utilisateur'] == user_id:
        abort(403)
    objets_utilisateur = utils.executeSqlQuery('SELECT * FROM objets WHERE id_utilisateur = %s', (user_id,))
    if request.method == 'POST':
        objet_a_troquer_id = int(request.form['objet_utilisateur'])
        objet_a_troquer = utils.executeSqlQuery('SELECT * FROM objets WHERE id = %s', (objet_a_troquer_id,))
        if objet_a_troquer is None or objet_a_troquer['id_utilisateur'] != user_id:
            flash("L'objet que vous avez sélectionné n'est pas valide.", "error")
        else:
            utils.executeSqlQuery('UPDATE objets SET id_utilisateur = %s WHERE id = %s',
                                  (objet['id_utilisateur'], objet_a_troquer_id))
            utils.executeSqlQuery('UPDATE objets SET id_utilisateur = %s WHERE id = %s', (user_id, id))
            flash("Échange effectué avec succès!", "success")
            return redirect(url_for('objet.objet'))
    return render_template('troquer_objet.jinja', objet=objet, objets_utilisateur=objets_utilisateur)

@bp_objet.route('/liste_objets', methods=['GET', 'POST'])
def liste_objets():
    recherche = request.args.get('recherche', '')
    query = "SELECT * FROM objets WHERE titre LIKE %s OR description LIKE %s"
    objets = utils.executeSqlQuery(query, (f"%{recherche}%", f"%{recherche}%"))
    return render_template('liste_objets.jinja', objets=objets, recherche=recherche)




